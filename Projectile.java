public interface Projectile {
    void beTossed(Person per, Target tar);
}