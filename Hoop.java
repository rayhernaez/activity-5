public class Hoop implements Target {
    private double position;

    //constructor
    public Hoop(double position) {
        this.position = position;
    }

    //getter
    public double getPosition() {
        return this.position;
    }

    //method
    public void receive(Projectile prj) {
        if (prj instanceof Basketball) {
            System.out.println("Got a basket!");
        }
        else {
            System.out.println("That wasn't a basketball!");;
        }
    }
}
