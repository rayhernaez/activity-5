public class Person {
    private String name;
    private double position;
    private Projectile heldProjectile;

    //constructor
    public Person(String name, double position) {
        this.name = name;
        this.position = position;
        this.heldProjectile = null;
    }

    //getters
    public String getName() {
        return this.name;
    }

    public double getPosition() {
        return this.position;
    }

    //methods
    public boolean giveProjectile(Projectile proj) {
        if (this.heldProjectile == null) {
            this.heldProjectile = proj;
            return true;
        }
        else {
            return false;
        }
    }

    public boolean toss(Target targ) {
        if (this.heldProjectile == null) {
            return false;
        }
        else {
            this.heldProjectile.beTossed(per, tar);; //object thingy?
        }
    }
}
