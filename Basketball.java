public class Basketball implements Projectile {
    private double posPer;
    private double posTar;
    private double distance;

    //method
    public void beTossed(Person per, Target tar) {
        this.posPer = per.getPosition();
        this.posTar = tar.getPosition();
        this.distance = this.posPer - this.posTar;

        if (this.distance <= 5) {
            tar.receive(); //
        }
        else {
            System.out.println("Missed the target.");
        }
    }
}
