public interface Target {
    void receive(Projectile prj);
    double getPosition();
}
